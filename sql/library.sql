SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `library` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `library` ;

-- -----------------------------------------------------
-- Table `library`.`Student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`Student` (
  `pkStudent` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NOT NULL,
  `studentId` VARCHAR(15) NULL,
  PRIMARY KEY (`pkStudent`),
  UNIQUE INDEX `pkStudent_UNIQUE` (`pkStudent` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library`.`Book`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`Book` (
  `pkBook` INT NOT NULL AUTO_INCREMENT,
  `isbn` VARCHAR(13) NOT NULL COMMENT 'Natural key',
  `title` VARCHAR(45) NOT NULL,
  `author` VARCHAR(100) NULL COMMENT 'Broken 1NF',
  `count` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`pkBook`),
  UNIQUE INDEX `pkBook_UNIQUE` (`pkBook` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library`.`Issued`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`Issued` (
  `pkIssued` INT NOT NULL AUTO_INCREMENT,
  `fkStudent` INT NOT NULL,
  `fkBook` INT NOT NULL,
  `start` DATE NULL,
  `end` DATE NULL,
  PRIMARY KEY (`pkIssued`),
  UNIQUE INDEX `pkIssued_UNIQUE` (`pkIssued` ASC),
  INDEX `fk_Issued_Student_idx` (`fkStudent` ASC),
  INDEX `fk_Issued_Book1_idx` (`fkBook` ASC),
  CONSTRAINT `fk_Issued_Student`
    FOREIGN KEY (`fkStudent`)
    REFERENCES `library`.`Student` (`pkStudent`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Issued_Book1`
    FOREIGN KEY (`fkBook`)
    REFERENCES `library`.`Book` (`pkBook`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library`.`Librarian`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`Librarian` (
  `pkLibrarian` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NULL,
  `password` VARCHAR(45) NULL,
  PRIMARY KEY (`pkLibrarian`),
  UNIQUE INDEX `pkLibrarian_UNIQUE` (`pkLibrarian` ASC))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

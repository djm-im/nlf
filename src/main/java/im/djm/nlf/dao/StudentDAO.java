package im.djm.nlf.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import im.djm.nlf.data.Student;

public class StudentDAO {

	private static StudentDAO instance = new StudentDAO();

	private StudentDAO() {
	}

	public static StudentDAO getInstance() {
		return instance;
	}

	public List<Student> findAll(EntityManager em) {
		Query query = em.createQuery("SELECT student FROM Student student");
		return query.getResultList();
	}

	public void add(EntityManager em, Student student) {
		em.persist(student);
	}

	public void edit(EntityManager em, Student student) {
		em.merge(student);
	}

	public void delete(EntityManager em, Student student) {
		Student managedStdent = em.merge(student);
		em.remove(managedStdent);
	}

}

package im.djm.nlf.dao;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.xml.bind.DatatypeConverter;

import im.djm.nlf.data.Librarian;
import im.djm.nlf.exceptions.LibraryException;

public class LibrarianDAO {

	private static LibrarianDAO instance = new LibrarianDAO();

	private LibrarianDAO() {
	}

	public static LibrarianDAO getInstance() {
		return instance;
	}

	public boolean checkCredentials(EntityManager em, Librarian librarian) throws LibraryException {
		try {
			if (doesUsernameExistInDatabase(em, librarian)) {
				// if user exist - check credentials
				fetchLibrarianFromDatabase(em, librarian);
				return true;
			} else {
				throw new LibraryException("Librarin with username: " + librarian.getUsername() + " already exists.");
			}
		} catch (NoResultException ex) {
			return false;
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException ex) {
			throw new LibraryException("Unkwnown username or password.", ex);
		}
	}

	public boolean createLibrarian(EntityManager em, Librarian librarian) throws LibraryException {
		try {
			if (!doesUsernameExistInDatabase(em, librarian)) {
				// Create the new librarian if user not exist

				Librarian newLibrarian = new Librarian(librarian.getUsername(), calculateHash(librarian));
				em.persist(newLibrarian);

				return true;
			}

			throw new LibraryException("Librarian with username " + librarian.getUsername() + " is already created.");
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException ex) {
			throw new LibraryException("Cannot create new librarian.", ex);
		}
	}

	private boolean doesUsernameExistInDatabase(EntityManager em, Librarian librarian) {
		Query query = em.createQuery("SELECT count(*) FROM Librarian WHERE username=:username");
		query.setParameter("username", librarian.getUsername());

		long cnt = (long) query.getSingleResult();
		return cnt > 0;
	}

	private Librarian fetchLibrarianFromDatabase(EntityManager em, Librarian librarian)
			throws UnsupportedEncodingException, NoSuchAlgorithmException {
		Query query = em
				.createQuery("SELECT lib FROM Librarian lib WHERE lib.username=:username AND lib.password=:password");
		query.setParameter("username", librarian.getUsername());
		query.setParameter("password", calculateHash(librarian));

		return (Librarian) query.getSingleResult();
	}

	private String calculateHash(Librarian librarian) throws UnsupportedEncodingException, NoSuchAlgorithmException {
		String usernameAndPassword = librarian.getUsername() + librarian.getPassword();

		MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
		byte hashBytes[] = messageDigest.digest(usernameAndPassword.getBytes("UTF-8"));

		return DatatypeConverter.printBase64Binary(hashBytes);
	}

}

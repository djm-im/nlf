package im.djm.nlf.data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Issued")
public class Issued implements Serializable {

	private static final long serialVersionUID = 3043101738838879787L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "pkIssued")
	private Integer pkIssued;

	@Column(name = "start")
	@Temporal(TemporalType.DATE)
	private Date start;

	@Column(name = "end")
	@Temporal(TemporalType.DATE)
	private Date end;

	@JoinColumn(name = "fkStudent", referencedColumnName = "pkStudent")
	@ManyToOne(optional = false)
	private Student fkStudent;

	@JoinColumn(name = "fkBook", referencedColumnName = "pkBook")
	@ManyToOne(optional = false)
	private Book fkBook;

	public Issued() {
	}

	public Issued(Integer pkIssued) {
		this.pkIssued = pkIssued;
	}

	public Issued(Date start, Student fkStudent, Book fkBook) {
		this.start = start;
		this.fkStudent = fkStudent;
		this.fkBook = fkBook;
	}

	public Integer getPkIssued() {
		return pkIssued;
	}

	public void setPkIssued(Integer pkIssued) {
		this.pkIssued = pkIssued;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public Student getFkStudent() {
		return fkStudent;
	}

	public void setFkStudent(Student fkStudent) {
		this.fkStudent = fkStudent;
	}

	public Book getFkBook() {
		return fkBook;
	}

	public void setFkBook(Book fkBook) {
		this.fkBook = fkBook;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (pkIssued != null ? pkIssued.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Issued)) {
			return false;
		}
		Issued other = (Issued) object;
		if ((this.pkIssued == null && other.pkIssued != null)
				|| (this.pkIssued != null && !this.pkIssued.equals(other.pkIssued))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Issued[pkIssued = " + pkIssued + "]";
	}

}

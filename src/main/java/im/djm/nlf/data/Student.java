package im.djm.nlf.data;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Student")
public class Student implements Serializable {

	private static final long serialVersionUID = -6122447100070080549L;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "fkStudent")
	private Collection<Issued> issuedCollection;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "pkStudent")
	private Integer pkStudent;

	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 45)
	@Column(name = "name")
	private String name;

	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 45)
	@Column(name = "surname")
	private String surname;

	@Size(max = 15)
	@Column(name = "studentId")
	private String studentId;

	public Student() {
	}

	public Student(Integer pkStudent) {
		this.pkStudent = pkStudent;
	}

	public Student(String name, String surname, String studentId) {
		this.name = name;
		this.surname = surname;
		this.studentId = studentId;
	}

	public Integer getPkStudent() {
		return pkStudent;
	}

	public void setPkStudent(Integer pkStudent) {
		this.pkStudent = pkStudent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (pkStudent != null ? pkStudent.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Student)) {
			return false;
		}

		Student other = (Student) object;
		if ((this.pkStudent == null && other.pkStudent != null)
				|| (this.pkStudent != null && !this.pkStudent.equals(other.pkStudent))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Student[pk = " + pkStudent + "]";
	}

	public Collection<Issued> getIssuedCollection() {
		return issuedCollection;
	}

	public void setIssuedCollection(Collection<Issued> issuedCollection) {
		this.issuedCollection = issuedCollection;
	}

}

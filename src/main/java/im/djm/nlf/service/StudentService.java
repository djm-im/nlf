package im.djm.nlf.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import im.djm.nlf.dao.EntityManagerWrapper;
import im.djm.nlf.dao.StudentDAO;
import im.djm.nlf.data.Student;
import im.djm.nlf.exceptions.LibraryException;

public class StudentService {

	private static final StudentService instance = new StudentService();

	private StudentService() {
	}

	public static StudentService getInstance() {
		return instance;
	}

	public List<Student> findAll() {
		return StudentDAO.getInstance().findAll(EntityManagerWrapper.getEntityManager());
	}

	public void add(Student student) throws LibraryException {
		validate(student);

		EntityManager em = EntityManagerWrapper.getEntityManager();
		try {
			em.getTransaction().begin();
			StudentDAO.getInstance().add(em, student);
			em.getTransaction().commit();
		} finally {
			em.close();
		}
	}

	public void edit(Student student) throws LibraryException {
		validate(student);

		EntityManager em = EntityManagerWrapper.getEntityManager();
		try {
			em.getTransaction().begin();
			StudentDAO.getInstance().edit(em, student);
			em.getTransaction().commit();
		} finally {
			em.close();
		}
	}

	public void delete(Student student) throws LibraryException {
		// validate input parameters (parameters those user entered)
		if (student.getPkStudent() == null) {
			throw new LibraryException("Unknown student ID.");
		}

		EntityManager em = EntityManagerWrapper.getEntityManager();
		try {
			em.getTransaction().begin();
			StudentDAO.getInstance().delete(em, student);
			em.getTransaction().commit();
		} finally {
			em.close();
		}
	}

	private void validate(Student student) throws LibraryException {
		List<String> errors = new ArrayList<>();
		if (student.getName() == null || student.getName().trim().isEmpty()) {
			errors.add("Parameter 'name' is required");
		}
		if (student.getSurname() == null || student.getSurname().trim().isEmpty()) {
			errors.add("Parameter 'surname' is required");
		}
		if (!errors.isEmpty()) {
			throw new LibraryException(errors);
		}
	}

}

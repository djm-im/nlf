package im.djm.nlf.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import im.djm.nlf.data.Book;
import im.djm.nlf.exceptions.LibraryException;
import im.djm.nlf.service.BookService;

public class BookServlet extends HttpServlet {

	private static final long serialVersionUID = -5999396802750857494L;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action = request.getParameter("action");
		ActionType actionType = ActionType.getFor(action);

		try {
			switch (actionType) {
			case LIST: {
				break;
			}
			case ADD: {
				Book book = createBook(request, ActionType.ADD);
				BookService.getInstance().add(book);
				break;
			}
			case EDIT: {
				Book book = createBook(request, ActionType.EDIT);
				BookService.getInstance().edit(book);
				break;
			}
			case DELETE: {
				Book book = createBook(request, ActionType.DELETE);
				BookService.getInstance().delete(book);
				break;
			}
			}
			if (actionType != ActionType.LIST) {
				request.setAttribute("message", "Action successfully completed.");
			}
		} catch (LibraryException ex) {
			request.setAttribute("errors", ex.getErrorMessages());
		}

		List<Book> books = BookService.getInstance().findAll();
		request.setAttribute("books", books);

		request.getRequestDispatcher("/WEB-INF/pages/book.jsp").forward(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	private Book createBook(HttpServletRequest request, ActionType actionType) throws UnsupportedEncodingException {

		Integer pkBook = null;
		if (actionType == ActionType.EDIT || actionType == ActionType.DELETE) {
			String pkBookParam = request.getParameter("pkBook");
			if (pkBookParam != null) {
				pkBook = Integer.parseInt(pkBookParam);
			}
		}

		String title = request.getParameter("title");
		String author = request.getParameter("author");
		String isbn = request.getParameter("isbn");
		String count = request.getParameter("count");

		Book book = new Book(isbn, title, author, Integer.parseInt(count));

		if (pkBook != null) {
			book.setPkBook(pkBook);
		}

		return book;
	}

}

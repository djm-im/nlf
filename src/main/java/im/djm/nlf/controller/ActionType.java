package im.djm.nlf.controller;

public enum ActionType {

    LIST("list"), ADD("add"), EDIT("edit"), DELETE("delete");

    private String action;

    private ActionType(String action) {
        this.action = action;
    }

    public static ActionType getFor(String action) {
        for (ActionType actionType : values()) {
            if (actionType.action.equals(action)) {
                return actionType;
            }
        }
        return LIST;
    }

}

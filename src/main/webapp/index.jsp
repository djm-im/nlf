<!DOCTYPE html>
<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
	<script type="text/javascript" src="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet" type="text/css">
</head>

<body style="padding-top: 80px;">
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" 
						data-target="#navbar-ex-collapse">
					<span class="sr-only">Toggle navigation</span> 
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand">NLF</a>
			</div>
			
			<div class="collapse navbar-collapse" id="navbar-ex-collapse">
				<a class="btn btn-default navbar-btn pull-right" data-toggle="modal" data-target="#modal-signin">
					<span class="glyphicon glyphicon-log-in"></span>
					Sign in
				</a>
				<a class="btn btn-default navbar-btn pull-right" data-toggle="modal" data-target="#modal-signup">
					Sign up
				</a>
			</div>
		</div>
	</div>

	<div class="section">
		<div class="container">
			<c:if test="${not empty errors}">
				<div class="row">
					<div class="col-md-12">

						<div class="alert alert-danger" role="alert">
							<c:forEach items="${errors}" var="error">
								<span class="glyphicon glyphicon-exclamation-sign"
									aria-hidden="true"></span>
								<span class="sr-only">Error:</span>
                                    ${error} <br />
							</c:forEach>
						</div>

					</div>
				</div>
			</c:if>

			<c:if test="${not empty message}">
				<div class="row">
					<div class="col-md-12">

						<div class="alert alert-success" role="alert">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Message:</span>
							${message}
						</div>
					</div>
				</div>
			</c:if>

			<div class="row">
				<div class="col-md-12">
					<div class="jumbotron">
						<h1>Library</h1>
						<p>Pleas login to see list of books, students and borrowed books</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ---------------------------------------------------------------------------------------------- -->
	
	<!-- modal sign in -->
	<div class="modal fade" id="modal-signin" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h4 class="modal-title">Sign in</h4>
				</div>

				<form class="form-horizontal" role="form" action="login" method="POST">
					<input type="hidden" name="action" value="signin" />

					<div class="modal-body">

						<div class="form-group">
							<div class="col-sm-2">
								<label for="username" class="control-label">Username</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="username"
									name="username" placeholder="Username">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<label for="password" class="control-label">Password</label>
							</div>
							<div class="col-sm-10">
								<input type="password" class="form-control" id="password"
									name="password" placeholder="Password">
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<a class="btn btn-default" data-dismiss="modal">Close</a>
						<button type="submit" class="btn btn-primary">Sign in</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<!-- modal sign up -->
	<div class="modal fade" id="modal-signup" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h4 class="modal-title">Sign up</h4>
				</div>

				<form class="form-horizontal" role="form" action="signup" method="POST">
					<input type="hidden" name="action" value="signup" />

					<div class="modal-body">
						<div class="form-group">
							<div class="col-sm-2">
								<label for="username" class="control-label">Username</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="username"
									name="username" placeholder="Username">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<label for="password1" class="control-label">Password</label>
							</div>
							<div class="col-sm-10">
								<input type="password" class="form-control" id="password1"
									name="password1" placeholder="Password">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<label for="password2" class="control-label">Password</label>
							</div>
							<div class="col-sm-10">
								<input type="password" class="form-control" id="password2" 
									name="password2" placeholder="Repeat Password">
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<a class="btn btn-default" data-dismiss="modal">Close</a>
						<button type="submit" class="btn btn-primary">Sign up</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>

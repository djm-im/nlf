<!DOCTYPE html>
<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
	<script type="text/javascript" src="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet" type="text/css">
</head>

<body style="padding-top: 80px;">
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand"><span>NLF</span></a>
			</div>

			<div class="collapse navbar-collapse" id="navbar-ex-collapse">
				<ul class="nav navbar-nav">
					<li><a href="student">Students</a></li>
					<li><a href="book">Books</a></li>
					<li class="active"><a href="issued">Issued</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right"></ul>
				<a class="btn btn-default navbar-btn pull-right" href="logout">
					Sign Out
				</a>
			</div>
		</div>
	</div>

	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-12"></div>
			</div>

			<c:if test="${not empty errors}">
				<div class="row">
					<div class="col-md-12">
						<div class="alert alert-danger" role="alert">
							<c:forEach items="${errors}" var="error">
								<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
								<span class="sr-only">Error:</span>
								${error} <br />
							</c:forEach>
						</div>
					</div>
				</div>
			</c:if>

			<c:if test="${not empty message}">
				<div class="row">
					<div class="col-md-12">
						<div class="alert alert-success" role="alert">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Message:</span>
							${message}
						</div>
					</div>
				</div>
			</c:if>

			<div class="row">
				<div class="col-md-12">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>Issued by</th>
								<th>Book / Author</th>
								<th>Since</th>
								<th>To</th>
								<th>Return to library</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${issues}" var="issued">
								<tr>
									<td id="td_${issued.pkIssued}">${issued.pkIssued}</td>
									<td id="td_ime_${issued.fkStudent.name}">${issued.fkStudent.name}
										${issued.fkStudent.surname}</td>
									<td id="td_naslov_${issued.fkBook.title}">${issued.fkBook.title}
										/ ${issued.fkBook.author}</td>

									<td id="td_od_${issued.start}">${issued.start}</td>
									<td id="td_od_${issued.end}">${issued.end}</td>
									<td><a class="btn btn-block btn-info" data-toggle="modal"
										data-target="#modal-rezduzi-delete"
										onclick="fillFields('${issued.pkIssued}', '${issued.fkStudent.pkStudent}', '${issued.fkBook.pkBook}', 'delete');">Return</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title">Borrow a book</h3>
						</div>
						<div class="panel-body">
							<form class="form-horizontal" role="form" action="issued"
								method="POST">
								<input type="hidden" name="action" value="add" />
								<div class="form-group">
									<div class="col-sm-2">
										<label for="inputEmail3" class="control-label">Student</label>
									</div>
									<div class="col-sm-10">
										<select name="fkStudent" class="form-control" id="sel2">
											<c:forEach items="${students}" var="student">
												<option value="${student.pkStudent}">${student.pkStudent}
													-- ${student.name} ${student.surname}</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-2">
										<label for="inputPassword3" class="control-label">Book</label>
									</div>
									<div class="col-sm-10">
										<select name="fkBook" class="form-control" id="sel2">
											<c:forEach items="${books}" var="book">
												<option value="${book.pkBook}">${book.title}</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
										<button type="submit" class="btn btn-info">Borrow</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- modal return borrowed book -->
	<div class="modal fade" id="modal-rezduzi-delete">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">x</button>
					<h4 class="modal-title">Return book</h4>

					<form class="" role="form" action="issued" method="POST">
						<input type="hidden" name="action" value="delete" /> <input
							type="hidden" id="pkIssued-delete" name="pkIssued" value="" /> <input
							type="hidden" id="fkStudent-delete" name="fkStudent" value="" /> <input
							type="hidden" id="fkBook-delete" name="fkBook" value="" />

						<div class="modal-footer">
							<a class="btn btn-default" data-dismiss="modal">Cancel</a>

							<button type="submit" class="btn btn-primary">Return</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<script>
		function fillFields(pkIssued, fkStudent, fkBook, action) {
			$("#pkIssued-" + action).val(pkIssued);
			$("#fkStudent-" + action).val(fkStudent);
			$("#fkBook-" + action).val(fkBook);
		}
	</script>
</body>

</html>

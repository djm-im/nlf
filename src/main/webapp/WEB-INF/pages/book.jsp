<!DOCTYPE html>
<%@page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
	<script type="text/javascript" src="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet" type="text/css">
</head>

<body style="padding-top: 80px;">
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target="#navbar-ex-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand"><span>NLF</span></a>
			</div>

			<div class="collapse navbar-collapse" id="navbar-ex-collapse">
				<ul class="nav navbar-nav">
					<li><a href="student">Students</a></li>
					<li class="active"><a href="book">Books</a></li>
					<li><a href="issued">Issued</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right"></ul>
				<a class="btn btn-default navbar-btn pull-right" href="logout">
					Sign Out
				</a>
			</div>
		</div>
	</div>
	
	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-12"></div>
			</div>

			<c:if test="${not empty errors}">
				<div class="row">
					<div class="col-md-12">

						<div class="alert alert-danger" role="alert">
							<c:forEach items="${errors}" var="error">
								<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
								<span class="sr-only">Error:</span>
								${error} <br />
							</c:forEach>
						</div>
					</div>
				</div>
			</c:if>

			<c:if test="${not empty message}">
				<div class="row">
					<div class="col-md-12">
						<div class="alert alert-success" role="alert">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Message:</span>
							${message}
						</div>
					</div>
				</div>
			</c:if>

			<div class="row">
				<div class="col-md-12">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>Book Title</th>
								<th>Author</th>
								<th>ISBN</th>
								<th>Count</th>
								<th>Edit</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${books}" var="book">
								<tr>
									<td id="td_${book.pkBook}">${book.pkBook}</td>
									<td id="td_title_${book.pkBook}">${book.title}</td>
									<td id="td_author_${book.pkBook}">${book.author}</td>
									<td id="td_isbn_${book.pkBook}">${book.isbn}</td>
									<td id="td_count_${book.pkBook}">${book.count}</td>
									<td>
										<a class="btn btn-block btn-info" data-toggle="modal"
												data-target="#modal-book-edit"
												onclick="fillFields('${book.pkBook}', 'edit');">
											Edit..
										</a>
									</td>
									<td>
										<a class="btn btn-block btn-danger" 
												data-toggle="modal" data-target="#modal-book-delete"
												onclick="fillFields('${book.pkBook}', 'delete');">
											Delete..
										</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>

					<a class="btn btn-success pull-right" data-toggle="modal" data-target="#modal-book-add">
						New Book..
					</a>
				</div>
			</div>
		</div>
	</div>


	<!-- modal new book -->
	<div class="modal fade" id="modal-book-add">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">x</button>
					<h4 class="modal-title">Data about book</h4>
				</div>

				<form class="form-horizontal" role="form" action="book" method="POST">
					<div class="modal-body">
						<input type="hidden" name="action" value="add" />

						<div class="form-group">
							<div class="col-sm-2">
								<label for="title" class="control-label">Title</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="title" placeholder="Book title" name="title">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<label for="author" class="control-label">Author</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="author" placeholder="Author" name="author">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<label for="isbn" class="control-label">ISBN</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="isbn" placeholder="ISBN" name="isbn">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<label for="count" class="control-label">Count</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="count" placeholder="Count" name="count">
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<a class="btn btn-default" data-dismiss="modal">Cancel</a>

						<button type="submit" class="btn btn-primary">Add</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- modal edit -->
	<div class="modal fade" id="modal-book-edit">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">x</button>
					<h4 class="modal-title">Edit data about book</h4>
				</div>

				<form class="form-horizontal" role="form" action="book" method="POST">
					<div class="modal-body">
						<input type="hidden" name="action" value="edit" />
						<input type="hidden" id="pkBook-edit" name="pkBook" value="" />

						<div class="form-group">
							<div class="col-sm-2">
								<label for="title" class="control-label">Title</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="title-edit" placeholder="Book title" name="title">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<label for="author" class="control-label">Author</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="author-edit" placeholder="Author" name="author">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<label for="isbn" class="control-label">ISBN</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="isbn-edit" placeholder="ISBN" name="isbn">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<label for="count" class="control-label">Count</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="count-edit" placeholder="Count" name="count">
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<a class="btn btn-default" data-dismiss="modal">Cancel</a>
						
						<button type="submit" class="btn btn-primary">Edit</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- modal delete -->
	<div class="modal fade" id="modal-book-delete">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">x</button>
					<h4 class="modal-title">Delete data about book</h4>

					<form class="" role="form" action="book" method="POST">
						<input type="hidden" name="action" value="delete" />
						<input type="hidden" id="pkBook-delete" name="pkBook" value="" />

						<input type="hidden" id="title-delete" name="title" value="" />
						<input type="hidden" id="author-delete" name="author" value="" />
						<input type="hidden" id="isbn-delete" name="isbn" value="" />
						<input type="hidden" id="count-delete" name="count" value="" />

						<div class="modal-footer">
							<a class="btn btn-default" data-dismiss="modal">Cancel</a>

							<button type="submit" class="btn btn-primary">Delete</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<script>
		function fillFields(pkBook, action) {
			$("#pkBook-" + action).val(pkBook);
			$("#title-" + action).val($("#td_title_" + pkBook).html());
			$("#author-" + action).val($("#td_author_" + pkBook).html());
			$("#isbn-" + action).val($("#td_isbn_" + pkBook).html());
			$("#count-" + action).val($("#td_count_" + pkBook).html());
		}
	</script>
</body>
</html>